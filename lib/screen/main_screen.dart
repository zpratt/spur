import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:spur/screen/parts/backgrounds.dart' as backgrounds;
import 'package:location/location.dart';
import 'dart:async';
import 'package:flutter/services.dart';

class MainScreen extends StatefulWidget {
  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> with SingleTickerProviderStateMixin {
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  AnimationController _animationController;
  GoogleMapController _mapController;

  static const _center = const LatLng(45.521563, -122.677433);

  void _onMapCreated(GoogleMapController controller) {
    _mapController = controller;
  }

  @override void initState() {
    _animationController = AnimationController(
      vsync: this,
      duration: const Duration(seconds: 30)
    );
    super.initState();

    _animationController.repeat();
  }

  @override
  Widget build(BuildContext context) {
    
    final screenSize = MediaQuery.of(context).size;

    final drawer = Drawer(
      child: Column(
        children: <Widget>[
          const DrawerHeader(
            child: const Text('Menu'),
          ),
          ListTile(
            leading: const Icon(Icons.settings),
            title: const Text('Settings'),
            onTap: () {
              Navigator.pop(context);
            },
          )
        ],
      ),
    );

    final spurButton = AnimatedBuilder(
      animation: _animationController,
        child: RaisedButton(
          padding: const EdgeInsets.all(5.0),
          child: const Icon(
            Icons.settings,
            color: Colors.white,
            size: 50.0,
          ),
          shape: const CircleBorder(),
          color: Colors.blue,
          onPressed: () {
            print('pressed');
          },
        ),
        builder: (context, _widget) {
          return Transform.rotate(
            angle: _animationController.value * 6.3,
            child: _widget,
          );
        },
      );

    final background = Container(
        width: screenSize.width,
        height: screenSize.height,
        decoration: backgrounds.login,
        alignment: Alignment.topCenter,
      );

    final appBar = Container(
      padding: const EdgeInsets.only(top: 20.0),
        decoration: const BoxDecoration(
          color: Colors.white,
        ),
        child: Row(
          children: <Widget>[
            IconButton(
              icon: const Icon(Icons.list),
              onPressed: () {
                _scaffoldKey.currentState.openDrawer();
              },
            )
          ],
        ),
      );

    final listView = Container(
      width: screenSize.width,
      height: 800.0,
      child: Column(
        children: <Widget>[
          Expanded(
            child: ListView.builder(
              padding: const EdgeInsets.all(15.0),
              itemCount: 20,
              itemBuilder: (context, i) {
                return Card(
                  child: Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: Text('Entry $i'),
                  ),
                );
              },
            ),
          )
        ],
      )
    );

    final map = GoogleMap(
      onMapCreated: _onMapCreated,
      initialCameraPosition: const CameraPosition(
        target: _center,
        zoom: 11.0,
      ),
    );

    Future<void> _getCurrentLocation() async {
      var location = Location();
      var currentLocation = <String, double>{};

      try {
        currentLocation = await location.getLocation();
      } on PlatformException {
        currentLocation = null;
      }

      if(currentLocation != null) {
        _mapController.moveCamera(CameraUpdate.newLatLng(LatLng(currentLocation['latitude'], currentLocation['longitude'])));
      }
    }

    final currentLocationButton = RaisedButton(
      child: Icon(
        Icons.my_location
      ),
      onPressed: _getCurrentLocation,
    );

    return Scaffold(
      key: _scaffoldKey,
      drawer: drawer,
      body: Stack(
        alignment: Alignment.topCenter,
        children: <Widget>[
          background,
          appBar,
/*          Positioned(
            top: 70.0,
            child: SizedBox(
              width: screenSize.width,
              height: 800.0,
              child: map,
            )
          ),*/
          Positioned(
            top: 70.0,
            child: listView,
          ),
          Positioned(
        top: 35.0,
            child: spurButton,
          ),
        ]
      )
    );
  }
}