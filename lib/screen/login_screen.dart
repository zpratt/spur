import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import 'package:spur/screen/parts/backgrounds.dart' as backgrounds;

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginData {
  String email = '';
  String password = '';
}

class _LoginScreenState extends State<LoginScreen> {
  final _formKey = GlobalKey<FormState>();
  var _data = new _LoginData();

  String _validatePassword(String password) {
    if(password.length < 8) {
      return 'Password must be at least 8 characters.';
    }

    return null;
  }

  String _validateEmail(String email) {
    if(!(email.length > 0 && email.contains('@') && email.contains('.'))) {
      return 'E-mail address must be a valid email address.';
    }

    return null;
  }

  bool _submit() {
    /*if(_formKey.currentState.validate()) {
      _formKey.currentState.save();
      http.post("http://www.spuryourlife.com/api/login",
          body: {"email": "${_data.email}", "password": "${_data.password}"})
          .then((response) {
            print('${response.statusCode}');
      });
    }*/

    return true;
  }

  @override
  Widget build(BuildContext context) {

    final screenSize = MediaQuery.of(context).size;

    final email = Container(
      child: TextFormField(
        keyboardType: TextInputType.emailAddress,
        decoration: const InputDecoration(
            border: InputBorder.none,
            labelText: 'Email',
            labelStyle: const TextStyle(color: Colors.white),
            hintStyle: const TextStyle(color: Colors.white),
            icon: const Icon(Icons.email, color: Colors.white, size: 20.0,)
        ),
        style: const TextStyle(color: Colors.white),
        validator: _validateEmail,
        onSaved: (value) {
          _data.email = value;
        },
      ),
    );

    final password = Container(
      child: TextFormField(
        obscureText: true,
        decoration: const InputDecoration(
            border: InputBorder.none,
            labelStyle: const TextStyle(color: Colors.white),
            labelText: 'Password',
            icon: const Icon(Icons.lock, color: Colors.white, size: 20.0,)
        ),
        style: const TextStyle(color: Colors.white),
        validator: _validatePassword,
        onSaved: (value) {
          _data.password = value;
        },
      ),
    );

    final loginButton = Container(
      margin: const EdgeInsets.only(left: 50.0, right: 50.0, top: 30.0),
      child: RaisedButton(
        child: const Text('Log In',
          style: const TextStyle(color: Colors.black)
        ),
        color: Colors.white,
        onPressed: () {
          if(_submit()) {
            Navigator.pushReplacementNamed(context, '/home');
          }
        },
      ),
    );

    return Scaffold(
      body: Container(
        decoration: backgrounds.login,
        child: Form(
          key: _formKey,
          child: Padding(
            padding: const EdgeInsets.only(left: 50.0, right: 50.0),
            child: ListView(
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(top: screenSize.height / 2)
                ),
                email,
                password,
                loginButton
              ],
            ),
          ),
        ),
      ),
    );
  }
}