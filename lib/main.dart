import 'package:flutter/material.dart';
import 'package:spur/screen/login_screen.dart';
import 'package:spur/screen/main_screen.dart';

void main() => runApp(SpurApp());

class SpurApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Spur',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      initialRoute: '/',
      routes: {
        '/': (context) => LoginScreen(),
        '/login': (context) => LoginScreen(),
        '/home': (context) => MainScreen(),
      },
    );
  }
}