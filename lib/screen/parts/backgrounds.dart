import 'package:flutter/material.dart';

const login = const BoxDecoration(
  gradient: const LinearGradient(
    begin: Alignment.centerLeft,
    end: const Alignment(1.0, 0.0),
    colors: [Color(0xff444152), Color(0xff6f6c7d)],
    tileMode: TileMode.repeated
  )
);